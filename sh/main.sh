#bin/bash

#FUNCTIONS

logo() {

    clear

    echo "
       ██╗   ██╗ ██████╗  ██╗
       ██║   ██║██╔════╝ ███║
       ██║   ██║██║  ███╗╚██║
       ██║   ██║██║   ██║ ██║
       ╚██████╔╝╚██████╔╝ ██║
        ╚═════╝  ╚═════╝  ╚═╝
       - Untitled Game One -
    "
    sleep 2s
    clear

}

playerName() {

    name=$(grep "name=" ../save/userinfo.sgf)
    name=${name#name=}

}

#MAIN

logo

playerName

if [[ $name == "" ]]; then

    read -p "Hello. I'm IRIS. Who are you? " newName
    echo name=$newName >> ../save/userinfo.sgf
    playerName

fi

echo "I'll be here to help you."
